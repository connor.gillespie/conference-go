import requests

OPEN_WEATHER_API_KEY = "bb85c122cfa4ec640befe2a4eb5cfdec"

city = "Philadelphia"
state = "PA"

url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}"
res = requests.get(url)

lat = res.json()[0]["lat"]
lon = res.json()[0]["lon"]

url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"

res = requests.get(url)
weather_description = res.json()["weather"][0]["description"]
current_temp = res.json()["main"]["temp"]
print(current_temp)
